export ZDOTDIR=$HOME/.config/zsh
source "$HOME/.config/zsh/.zshrc"

neofetch

# Generated for envman. Do not edit.
[ -s "$HOME/.config/envman/load.sh" ] && source "$HOME/.config/envman/load.sh"

PATH="$HOME/.local/bin${PATH:+:${PATH}}"

